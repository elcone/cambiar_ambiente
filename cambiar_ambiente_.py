#!/usr/bin/env python
import sys, os
import cmd
import zipfile
from datetime import datetime

class Consola(cmd.Cmd):
    def __init__(self):
        super(Consola, self).__init__()
        self.cambia_ambiente = CambiarAmbiente()
        self.prompt = 'kup >> '

    def do_cambiar(self, args):
        try:
            self.cambia_ambiente.seleccionar_ambiente()
        except:
            print('Operación cancelada')

    def do_respaldar(self, args):
        archivo = 'respaldo_ambiente_{0}_{1}.zip'.format(
            self.cambia_ambiente.ambiente_activo,
            datetime.today().strftime('%Y-%m-%d'))
        
        zip_ = zipfile.ZipFile(os.path.join('c:/kernelup', archivo), 'w')
        zip_.write('c:/kernelup/servidor/')
        zip_.write('c:/kernelup/programacion/')
        zip_.close()

    def do_activo(self, args):
        print(self.cambia_ambiente.ambiente_activo)

    def do_salir(self, args):
        return -1

    def do_ayuda(self, args):
        print('cambiar, respaldar, activo, salir')

    def emptyline(self):
        pass


class CambiarAmbiente:
    """
    Los diferentes ambientes deben tener los siguientes nombres:

    @EMPRESA-programacion
    @EMPRESA-servidor
    """
    def __init__(self):
        self.path_raiz = 'c:/kernelup'
        self.path_ambiente_activo = os.path.join(self.path_raiz,
            'ambiente_activo.txt')
        self.ambientes = []
        self.ambiente_activo = self.confirmar_ambiente_activo()
        self.ambiente_destino = None
        self.cargar_ambientes()

    def confirmar_ambiente_activo(self):
        """
        Regresa el nombre del ambiente activo,
        el nombre es tomado del archivo 'c:/kernelup/ambiente_activo.txt'

        Si el archivo no está presente se crea y se pide el nombre del ambiente
        """
        self.validar_ambiente()
        if os.path.isfile(self.path_ambiente_activo):
            archivo_ambiente_activo = open(self.path_ambiente_activo, 'r')
            ambiente_activo = archivo_ambiente_activo.readlines()[0]
        else:
            ambiente_activo = input('Ambiente activo: ').upper()
            archivo_ambiente_activo = open(self.path_ambiente_activo, 'w')
            archivo_ambiente_activo.write(ambiente_activo)

        archivo_ambiente_activo.close()
        return ambiente_activo

    def cargar_ambientes(self):
        """
        Busca todos los posibles ambientes en la carpera 'c:/kernelup'
        """
        carpetas = os.listdir(self.path_raiz)
        ambientes = [x for x in carpetas if x[0] == '@']

        """
        Se crea un 'set' para eliminar los elementos duplicados y se vuelve a
        transformar en un 'list'

        Los elementos duplicados son los nombre de los ambientes, se repieten
        al estar presentes en las carpetas 'programación' y 'servidor'
        """
        self.ambientes = sorted(list(set([x[1:x.index('-')] for x in ambientes])))

    def validar_ambiente(self, ambiente=None):
        """
        Valida la presencia de un ambiente, si no se recibe el parámetro
        'ambiente' se revisa la existencia de las carpetas 'programación' y
        'servidor', de lo contrario se revisan las capetas
        '@AMBIENTE-programacion' y '@AMBIENTE-servidor'
        """
        if ambiente:
            programacion = os.path.join(self.path_raiz,
                '@' + ambiente + '-programacion')
            servidor = os.path.join(self.path_raiz,
                '@' + ambiente + '-servidor')
        else:
            programacion = os.path.join(self.path_raiz, 'programacion')
            servidor = os.path.join(self.path_raiz, 'servidor')

        assert os.path.isdir(programacion), 'No existe la carpeta ' + programacion
        assert os.path.isdir(servidor), 'No existe la carpeta ' + servidor

        return True

    def seleccionar_ambiente(self):
        """
        Muestra en pantalla los ambientes encontrados y pide seleccionar alguno
        de ellos
        """
        print('Ambiente activo:', self.ambiente_activo)
        for x, y in enumerate(self.ambientes):
            print(x, '-', y)

        respuesta = input('Selecciona un ambiente: ')
        try:
            respuesta = int(respuesta)
        except:
            respuesta = -1

        assert respuesta in range(len(self.ambientes)), 'Eso no es un ambiente'

        self.ambiente_destino = self.ambientes[respuesta]
        if self.validar_ambiente(self.ambiente_destino) and self.validar_ambiente():
            self.cambiar_ambiente()
            self.ambiente_activo = self.confirmar_ambiente_activo()
            self.cargar_ambientes()

    def cambiar_ambiente(self):
        """
        Renombra las carpetas de acuerdo al ambiente seleccionado y escribe el
        nombre del nuevo ambiente activo en el archivo
        'c:/kernelup/ambiente_activo.txt'
        """
        print(self.ambiente_activo, '->', self.ambiente_destino)

        programacion_activo_origen = os.path.join(self.path_raiz, 'programacion')
        servidor_activo_origen = os.path.join(self.path_raiz, 'servidor')

        programacion_activo_destino = os.path.join(self.path_raiz,
            '@' + self.ambiente_activo + '-programacion')
        servidor_activo_destino = os.path.join(self.path_raiz,
            '@' + self.ambiente_activo + '-servidor')

        programacion_destino_origen = os.path.join(self.path_raiz,
            '@' + self.ambiente_destino + '-programacion')
        servidor_destino_origen = os.path.join(self.path_raiz,
            '@' + self.ambiente_destino + '-servidor')

        programacion_destino_destino = programacion_activo_origen
        servidor_destino_destino = servidor_activo_origen

        os.rename(programacion_activo_origen, programacion_activo_destino)
        os.rename(servidor_activo_origen, servidor_activo_destino)
        os.rename(programacion_destino_origen, programacion_destino_destino)
        os.rename(servidor_destino_origen, servidor_destino_destino)

        with open(self.path_ambiente_activo, 'w') as archivo:
            archivo.write(self.ambiente_destino)


def main():
    #cambia_ambiente = CambiarAmbiente()
    #try:
        #cambia_ambiente.seleccionar_ambiente()
    #except:
        #print('Error:', sys.exc_info()[1])

    #os.system('pause')
    consola = Consola()
    consola.cmdloop()


if __name__ == '__main__':
    main()
    sys.exit(0)
